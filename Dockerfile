FROM debian:buster-slim

# install base depedencies
RUN set -xe \
    && mkdir -p /usr/share/man/man1 \
    && apt-get update \
    && apt-get install --no-install-recommends --yes \
        curl \
        bzip2 \
        file \
        locales \
        make \
        default-jdk-headless \
        unzip \
        xz-utils \
    && rm -r /var/lib/apt/lists/* \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale en_US.UTF-8

ENV \
    ANDROID_SDK=/opt/android/sdk \
    ANDROID_HOME=/opt/android/sdk \
    ANDROID_SDK_ROOT=/opt/android/sdk \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

# install android sdk tools
RUN set -xe \
    && SDK_TOOLS_URL=`curl https://developer.android.com/studio/ \
        | egrep -o "https://dl.google.com/android/repository/commandlinetools-linux-.+?\.zip"` \
    && SDK_TOOLS_NAME=`basename ${SDK_TOOLS_URL}` \
    && curl ${SDK_TOOLS_URL} -o /tmp/${SDK_TOOLS_NAME} \
    && mkdir -p ${ANDROID_SDK} \
    && unzip /tmp/${SDK_TOOLS_NAME} -d ${ANDROID_SDK} \
    && rm -f /tmp/${SDK_TOOLS_NAME}

ENV PATH=${PATH}:${ANDROID_SDK}/cmdline-tools/bin:${ANDROID_SDK}/tools:${ANDROID_SDK}/tools/bin:${ANDROID_SDK}/platform-tools

ARG ANDROID_COMPONENTS="platform-tools \
    build-tools;31.0.0 platforms;android-31 \
    ndk-bundle \
    "

ARG GOOGLE_COMPONENTS=""

# install android components
RUN set -xe \
    && SDKMANAGER="sdkmanager --sdk_root=${ANDROID_HOME}" \
    && yes | ${SDKMANAGER} --licenses | grep -v = || true \
    && for COMPONENT in ${ANDROID_COMPONENTS}; do ${SDKMANAGER} ${COMPONENT} | grep -v = || true; done \
    && for COMPONENT in ${GOOGLE_COMPONENTS}; do ${SDKMANAGER} ${COMPONENT} | grep -v = || true; done \
    && ${SDKMANAGER} --update | grep -v = || true

CMD ["adb"]
